import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import upskill.java232.urusova.sorting.NewSortingApp;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;



@RunWith(Parameterized.class)
public class NewSortingAppTest {
    private static final Logger logger = LoggerFactory.getLogger(NewSortingAppTest.class);

    private final String[] inputArgs;
    private final String expectedOutput;

    public NewSortingAppTest(String[] inputArgs, String expectedOutput) {
        this.inputArgs = inputArgs;
        this.expectedOutput = expectedOutput;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSortWithZeroArray() {
        NewSortingApp.main(new String[]{});
    }

    @Test (expected = IllegalArgumentException.class)
    public void testSortAboveMax() {
        NewSortingApp.main(new String[]{"6","5","7","1","5","3","6","9","7","4","4"});
    }


    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"5"}, "5"},
                {new String[]{"9", "3", "7", "1", "5", "2", "8", "4", "6", "0"}, "0 1 2 3 4 5 6 7 8 9"},
        });
    }

    @Test
    public void testNewSortingApp() {

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        try {
            NewSortingApp.main(inputArgs);
            String actualOutput = outContent.toString().trim();
            Assert.assertEquals(expectedOutput, actualOutput);
        } finally {
            System.setOut(System.out);

        }

    }}



