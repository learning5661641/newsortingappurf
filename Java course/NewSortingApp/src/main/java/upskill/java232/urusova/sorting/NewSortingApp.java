package upskill.java232.urusova.sorting;

import java.util.Arrays;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class NewSortingApp {
    private static final Logger logger = LoggerFactory.getLogger(NewSortingApp.class);

    public static void main(String[] args) {

        if (args.length == 0) {
            throw new IllegalArgumentException("Less than 1 integer provided");
        } else if (args.length > 10) {
            throw new IllegalArgumentException("More than 10 integers provided");
        }

        int[] numbers = new int[args.length];
        try {
            for (int i = 0; i < args.length; i++) {
                numbers[i] = Integer.parseInt(args[i]);
            }

            bubbleSort(numbers);
            logger.info("Array after sorting: {}", Arrays.toString(numbers));

                for (int number : numbers) {
                    System.out.print(number + " ");}
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Error: Not a valid integer provided.", e);
        }
    }

    private static void bubbleSort(int[] arr) {
        int n = arr.length;
        boolean swapped;
        do {
            swapped = false;
            for (int i = 1; i < n; i++) {
                if (arr[i - 1] > arr[i]) {
                    int temp = arr[i - 1];
                    arr[i - 1] = arr[i];
                    arr[i] = temp;
                    swapped = true;
                }
            }
            n--;
        } while (swapped);


    }
}
